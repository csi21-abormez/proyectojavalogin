package com.spring.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.spring.models.Activity;
import com.spring.models.Booking;
import com.spring.services.BookingServices;

@Controller
public class BookingController {
	
	@Autowired
	BookingServices bookingServices;
	
	 @RequestMapping("bookingHome")
	 public ModelAndView home() {
	 List<Booking> listBooking = bookingServices.listAll();
	 ModelAndView mav = new ModelAndView("booking/index");
	 mav.addObject("listBooking", listBooking);
	 return mav;
	 }

	 @RequestMapping("bookingNew")
	 public String newBookingForm(Map<String, Object> model) {
		 Booking booking = new Booking();
	  model.put("booking", booking);
	  return "booking/new_booking";
	 }
	 
	 @RequestMapping(value = "bookingSave", method = RequestMethod.POST)
	 public String saveBooking(@ModelAttribute("booking") Booking booking) {
		 bookingServices.save(booking);
	  return "redirect:/bookingHome";
	 }
	 
	 @RequestMapping("bookingEdit")
	 public ModelAndView editBookingForm(@RequestParam long id) {
		 ModelAndView mav = new ModelAndView("booking/edit_booking");
		 Booking booking = bookingServices.get(id);
		 mav.addObject("booking",booking);
		 
		 return mav;
		 
	 }
	 
	 @RequestMapping("bookingDelete")
	 public String deleteBookingForm(@RequestParam long id) {
		 bookingServices.delete(id);
	  return "redirect:/bookingHome";
	 }

	 @RequestMapping("bookingSearch")
	 public ModelAndView search(@RequestParam String keyword) {
	  List<Booking> result = bookingServices.search(keyword);
	  ModelAndView mav = new ModelAndView("booking/search");
	  mav.addObject("result", result);
	  return mav;
	 }

}
