package com.spring.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.spring.models.Customer;
import com.spring.models.Trainer;
import com.spring.services.CustomerServices;
import com.spring.services.TrainerServices;

@Controller
public class LoginController {
	


	@Autowired
	TrainerServices trainerServices;
	@Autowired
	CustomerServices customerServices;
	@RequestMapping
	public String inicio() {
		return "login";
	}
	
	@RequestMapping("searchLogin")
		public ModelAndView search(@RequestParam String nick, String password, String rol) {
			ModelAndView modelAndView = new ModelAndView("entrada-error");
			if(rol.equals("trainer")) {
				
				Trainer trainer = trainerServices.dameTrainerLogado(nick, password);
				if(trainer == null) {
					modelAndView = new ModelAndView("trainer/entrada-error-trainer");
				}
				if(trainer != null) {
					List<Trainer> result = trainerServices.listAll();
					if(result.size()!=0) {
						modelAndView = new ModelAndView("trainer/index");
						modelAndView.addObject("listTrainer", result);
					}
				}
				
			}else if(rol.equals("customer")){
				Customer customer = customerServices.dameCustomerLogado(nick, password);
				if(customer != null) {
					
					System.out.println(customer.toString());
					List<Customer> result = customerServices.listAll();
					if(result.size() != 0) {
						modelAndView = new ModelAndView("customer/index");
						modelAndView.addObject("listCustomer", result);
					}
				}else if(customer == null) {
					modelAndView = new ModelAndView("customer/entrada-error-customer");
				
			
		}
			
	}
			return modelAndView;
}

}
