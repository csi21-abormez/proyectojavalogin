package com.spring.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.spring.models.Activity;
import com.spring.models.Customer;
import com.spring.services.ActivityServices;
import com.spring.services.CustomerServices;

@Controller
public class ActivityController {
	
	@Autowired
	 private ActivityServices activityServices;
	
	 @RequestMapping("activityHome")
	 public ModelAndView home() {
	 List<Activity> listActivity = activityServices.listAll();
	 ModelAndView mav = new ModelAndView("activity/index");
	 mav.addObject("listActivity", listActivity);
	 return mav;
	 }

	 @RequestMapping("activityNew")
	 public String newActivityForm(Map<String, Object> model) {
	  Activity activity = new Activity();
	  model.put("activity", activity);
	  return "activity/new_activity";
	 }
	 
	 @RequestMapping(value = "activitySave", method = RequestMethod.POST)
	 public String saveActivity(@ModelAttribute("activity") Activity activity) {
		 activityServices.save(activity);
	  return "redirect:/activityHome";
	 }
	 
	 @RequestMapping("activityEdit")
	 public ModelAndView editActivityform(@RequestParam long id) {
		 ModelAndView mav = new ModelAndView("activity/edit_activity");
		 Activity activity = activityServices.get(id);
		 mav.addObject("activity",activity);
		 
		 return mav;
		 
	 }
	 
	 @RequestMapping("activityDelete")
	 public String deleteActivityForm(@RequestParam long id) {
	  activityServices.delete(id);
	  return "redirect:/activityHome";
	 }

	 @RequestMapping("activitySearch")
	 public ModelAndView search(@RequestParam String keyword) {
	  List<Activity> result = activityServices.search(keyword);
	  ModelAndView mav = new ModelAndView("activity/search");
	  mav.addObject("result", result);
	  return mav;
	 }

}
