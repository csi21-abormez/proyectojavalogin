package com.spring.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.spring.models.Customer;
import com.spring.models.Trainer;
import com.spring.services.TrainerServices;

@Controller
public class TrainerController {
	 @Autowired
	 private TrainerServices trainerServices;
	 @RequestMapping("trainerHome")
	 public ModelAndView home() {
	 List<Trainer> listTrainer = trainerServices.listAll();
	 ModelAndView mav = new ModelAndView("trainer/index");
	 mav.addObject("listTrainer", listTrainer);
	 return mav;
	 }

	 @RequestMapping("trainerNew")
	 public String newTrainerForm(Map<String, Object> model) {
	  Trainer trainer = new Trainer();
	  model.put("trainer", trainer);
	  return "trainer/new_trainer";
	 }
	 
	 @RequestMapping(value = "trainerSave", method = RequestMethod.POST)
	 public String saveTrainer(@ModelAttribute("trainer") Trainer trainer) {
	  trainerServices.save(trainer);
	  return "redirect:/trainerHome";
	 }
	 
	 @RequestMapping("trainerEdit")
	 public ModelAndView editTrainerForm(@RequestParam long id) {
		 ModelAndView mav = new ModelAndView("trainer/edit_trainer");
		 Trainer trainer = trainerServices.get(id);
		 mav.addObject("trainer", trainer);
		 
		 return mav;
		 
	 }
	 
	 @RequestMapping("trainerDelete")
	 public String deleteTrainerForm(@RequestParam long id) {
	  trainerServices.delete(id);
	  return "redirect:/home";
	 }

	 @RequestMapping("trainerSearch")
	 public ModelAndView search(@RequestParam String keyword) {
	  List<Trainer> result = trainerServices.search(keyword);
	  ModelAndView mav = new ModelAndView("trainer/search");
	  mav.addObject("result", result);
	  return mav;
	 }

}
