package com.spring.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.spring.models.Activity;
import com.spring.models.Room;
import com.spring.repository.RoomRepository;
import com.spring.services.RoomServices;

@Controller
public class RoomController {

	@Autowired
	 private RoomServices roomServices;
	
	 @RequestMapping("roomHome")
	 public ModelAndView home() {
	 List<Room> listRoom = roomServices.listAll();
	 ModelAndView mav = new ModelAndView("room/index");
	 mav.addObject("listRoom", listRoom);
	 return mav;
	 }

	 @RequestMapping("roomNew")
	 public String newRoomForm(Map<String, Object> model) {
		 Room room = new Room();
	  model.put("room", room);
	  return "room/new_room";
	 }
	 
	 @RequestMapping(value = "roomSave", method = RequestMethod.POST)
	 public String saveRoom(@ModelAttribute("room") Room room) {
		 roomServices.save(room);
	  return "redirect:/roomHome";
	 }
	 
	 @RequestMapping("roomEdit")
	 public ModelAndView editRoomform(@RequestParam long id) {
		 ModelAndView mav = new ModelAndView("room/edit_room");
		 Room room = roomServices.get(id);
		 mav.addObject("room",room);
		 
		 return mav;
		 
	 }
	 
	 @RequestMapping("roomDelete")
	 public String deleteRoomForm(@RequestParam long id) {
		 roomServices.delete(id);
	  return "redirect:/roomHome";
	 }

	 @RequestMapping("roomSearch")
	 public ModelAndView search(@RequestParam String keyword) {
	  List<Room> result = roomServices.search(keyword);
	  ModelAndView mav = new ModelAndView("room/search");
	  mav.addObject("result", result);
	  return mav;
	 }
}
