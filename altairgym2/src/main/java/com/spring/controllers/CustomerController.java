package com.spring.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.spring.models.Customer;
import com.spring.services.CustomerServices;

@Controller
public class CustomerController {
	 @Autowired
	 private CustomerServices customerService;
	 @RequestMapping("home")
	 public ModelAndView home() {
	 List<Customer> listCustomer = customerService.listAll();
	 ModelAndView mav = new ModelAndView("customer/index");
	 mav.addObject("listCustomer", listCustomer);
	 return mav;
	 }

	 @RequestMapping("new")
	 public String newCustomerForm(Map<String, Object> model) {
	  Customer customer = new Customer();
	  model.put("customer", customer);
	  return "customer/new_customer";
	 }
	 
	 @RequestMapping(value = "save", method = RequestMethod.POST)
	 public String saveCustomer(@ModelAttribute("customer") Customer customer) {
	  customerService.save(customer);
	  return "redirect:/home";
	 }
	 
	 @RequestMapping("edit")
	 public ModelAndView editCustomerform(@RequestParam long id) {
		 ModelAndView mav = new ModelAndView("customer/edit_customer");
		 Customer customer = customerService.get(id);
		 mav.addObject("customer", customer);
		 
		 return mav;
		 
	 }
	 
	 @RequestMapping("delete")
	 public String deleteCustomerForm(@RequestParam long id) {
	  customerService.delete(id);
	  return "redirect:/home";
	 }

	 @RequestMapping("search")
	 public ModelAndView search(String keyword) {
	  List<Customer> result = customerService.search(keyword);
	  ModelAndView mav = new ModelAndView("customer/search");
	  mav.addObject("result", result);
	  return mav;
	 }
	 
	 
	 
}
