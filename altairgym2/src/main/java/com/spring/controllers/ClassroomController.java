package com.spring.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.spring.models.Booking;
import com.spring.models.Classroom;
import com.spring.services.ClassroomServices;

@Controller
public class ClassroomController {

	@Autowired
	ClassroomServices classroomServices;
	
	@RequestMapping("classroomHome")
	 public ModelAndView home() {
	 List<Classroom> listClassroom = classroomServices.listAll();
	 ModelAndView mav = new ModelAndView("classroom/index");
	 mav.addObject("listClassroom", listClassroom);
	 return mav;
	 }

	 @RequestMapping("classroomNew")
	 public String newBookingForm(Map<String, Object> model) {
		 Classroom classroom = new Classroom();
	  model.put("classroom", classroom);
	  return "classroom/new_classroom";
	 }
	 
	 @RequestMapping(value = "classroomSave", method = RequestMethod.POST)
	 public String saveClassroom(@ModelAttribute("classroom") Classroom classroom) {
		 classroomServices.save(classroom);
	  return "redirect:/classroomHome";
	 }
	 
	 @RequestMapping("classroomEdit")
	 public ModelAndView editClassroomForm(@RequestParam long id) {
		 ModelAndView mav = new ModelAndView("classroom/edit_classroom");
		 Classroom classroom = classroomServices.get(id);
		 mav.addObject("classroom",classroom);
		 
		 return mav;
		 
	 }
	 
	 @RequestMapping("classroomDelete")
	 public String deleteClassroomForm(@RequestParam long id) {
		 classroomServices.delete(id);
	  return "redirect:/bookingHome";
	 }

	 @RequestMapping("classroomSearch")
	 public ModelAndView search(@RequestParam String keyword) {
	  List<Classroom> result = classroomServices.search(keyword);
	  ModelAndView mav = new ModelAndView("classroom/search");
	  mav.addObject("result", result);
	  return mav;
	 }
}
