package com.spring.models;

import java.util.Date;
import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
@Table(name = "Classroom")
public class Classroom extends Person {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2671431641797797419L;
	
	private Date classDate;
	private Date hour;
	
	public Classroom(Date classDate, Date hour) {
		super();
		this.classDate = classDate;
		this.hour = hour;
	}
	public Classroom() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Date getClassDate() {
		return classDate;
	}
	public void setClassDate(Date classDate) {
		this.classDate = classDate;
	}
	public Date getHour() {
		return hour;
	}
	public void setHour(Date hour) {
		this.hour = hour;
	}
		
	// RELATIONS 
	private List<Booking> bookings;
	private Activity activity;
	private Room room;
	
	@OneToMany(mappedBy = "classroom")
	@Valid
	@NotNull
	public List<Booking> getBookings() {
		return bookings;
	}
	public void setBookings(List<Booking> bookings) {
		this.bookings = bookings;
	}
	
	@ManyToOne(optional = false)
	@Valid
	@NotNull
	public Activity getActivity() {
		return activity;
	}
	public void setActivity(Activity activity) {
		this.activity = activity;
	}
	
	@ManyToOne(optional = false)
	@Valid
	@NotNull
	public Room getRoom() {
		return room;
	}
	public void setRoom(Room room) {
		this.room = room;
	}
	
	
}

