package com.spring.models;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

	@Entity
	@Access(AccessType.PROPERTY)
	@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
	public abstract class Person extends DomainEntity {

		/**
		 * 
		 */
		private static final long serialVersionUID = -8956301772731815372L;
		public String name;
		public String surname;
		public String email;
		public String phone;
		public int age;
		public String nick;
		public String password;
		public String rol;
		
		public Person() {
			name = null;
			surname = null;
			email = null;
			phone = null;
			age = 0;
			nick ="";
			password="";
			rol ="";
		}
		
		public Person(String name, String surname, String email, String phone, int age, String nick, String password,String rol) {
			super();
			this.name = name;
			this.surname = surname;
			this.email = email;
			this.phone = phone;
			this.age = age;
			this.nick = nick;
			this.password = password;
			this.rol = rol;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getSurname() {
			return surname;
		}
		public void setSurname(String surname) {
			this.surname = surname;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getPhone() {
			return phone;
		}
		@Min(9)
		public void setPhone(String phone) {
			this.phone = phone;
		}
		@Min(value = 16)
		@Max(value = 99)
		public int getAge() {
			return age;
		}
		public void setAge(int age) {
			this.age = age;
		}

		public String getNick() {
			return nick;
		}

		public void setNick(String nick) {
			this.nick = nick;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getRol() {
			return rol;
		}

		public void setRol(String rol) {
			this.rol = rol;
		}
		
	}



