package com.spring.models;

import java.util.List;

import javax.management.monitor.Monitor;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Required;

@Entity
@Access(AccessType.PROPERTY)
@Table(name = "Activity")
public class Activity extends DomainEntity {
	

	
		/**
	 * 
	 */
	private static final long serialVersionUID = 8910389049372257132L;
	
		private String title;
		private String description;
		private int numberAssistants;
		private int duration;
		private boolean active;


		
		public Activity(String title, String description, int numberAssistants, int duration, boolean active) {
			super();
			this.title = title;
			this.description = description;
			this.numberAssistants = numberAssistants;
			this.duration = duration;
			this.active = active;
		}
		public Activity() {
			super();
			// TODO Auto-generated constructor stub
		}
		
		@NotBlank
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		@NotNull
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		@NotNull
		public int getNumberAssistants() {
			return numberAssistants;
		}
		public void setNumberAssistants(int numberAssistants) {
			this.numberAssistants = numberAssistants;
		}
		@NotNull
		public int getDuration() {
			return duration;
		}
		public void setDuration(int duration) {
			this.duration = duration;
		}
		@Required
		public boolean isActive() {
			return active;
		}
		public void setActive(boolean active) {
			this.active = active;
		}
		
		//Relationships
	private List<Classroom> classrooms;
	private Trainer trainer;


	@Valid
	@OneToMany(mappedBy = "activity")
	public List<Classroom> getClassrooms() {
		return classrooms;
	}
	public void setClassrooms(List<Classroom> classrooms) {
		this.classrooms = classrooms;
	}
	@NotNull
	@Valid
	@ManyToOne(optional = false)
	public Trainer getTrainer() {
		return trainer;
	}
	public void setTrainer(Trainer trainer) {
		this.trainer = trainer;
	}
	

}
