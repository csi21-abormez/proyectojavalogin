package com.spring.models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.Valid;

@Entity
@Table(name = "Customer")
public class Customer extends Person {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1501615797080492153L;

	private int number;
	private int weight;
	private int height;
	
	public Customer(int ID, String name, String surname, String email, String phone, 
			int age, int number, int weight,int height, String nick, String password, String rol) {
		super(name, surname, email, phone, age, nick, password, rol);
		this.height = height;
		this.number = number;
		this.weight = weight;
		
	}
	public Customer() {
		name = null;
		surname = null;
		email = null;
		phone = null;
		age = 0;
		nick = null;
		password=null;
		rol = null;
		number = 0;
		weight = 0;
		height = 0;
	}
	
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}

	//Relationships
	
		private List<Booking> bookings;
	   @Valid
	   @OneToMany(mappedBy = "customer")
		public List<Booking> getBookings() {
			return bookings;
		}


		public void setBookings(List<Booking> bookings) {
			this.bookings = bookings;
		}
	
}
