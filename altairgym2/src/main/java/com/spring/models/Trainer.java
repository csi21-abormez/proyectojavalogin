package com.spring.models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.Valid;
@Entity
@Table(name = "Trainer")
public class Trainer extends Person {
	
	
	    
		/**
	 * 
	 */
	private static final long serialVersionUID = -1411641988973571542L;
	
	private Long id;
	
		public Trainer(String name, String surname, String email, String phone, int age, String nick, String password, String rol) {
			super(name, surname, email, phone, age,nick, password, rol);
			
		}
		
		public Trainer() {
			name = null;
			surname = null;
			email = null;
			phone = null;
			age = 0;
			nick = null;
			password=null;
			rol= null;
			
		}

		
		//Relationships
		
		private List<Activity> activities;
		@Valid
		@OneToMany(mappedBy = "trainer")
		public List<Activity> getActivities() {
			return activities;
		}

		public void setActivities(List<Activity> activities) {
			this.activities = activities;
		}

	}
		
		
	



