package com.spring.models;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
@Table(name = "Booking")
public class Booking extends DomainEntity{
 /**
	 * 
	 */
	private static final long serialVersionUID = 8564779967565043355L;
	
public Date bookingDate;

 
 public Booking() {
	 super();
 }

 @Past
 @Temporal(TemporalType.TIMESTAMP)
 @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
public Date getBookingDate() {
	return bookingDate;
}
public void setBookingDate(Date bookingDate) {
	bookingDate = bookingDate;
}
 
 
//Relationships
private Customer customer;
private Classroom classroom;

@NotNull
@Valid
@ManyToOne(optional = false)
public Customer getCustomer() {
	return customer;
}

public void setCustomer(Customer customer) {
	this.customer = customer;
}
@NotNull
@Valid
@ManyToOne(optional = false)
public Classroom getClassroom() {
	return classroom;
}

public void setClassroom(Classroom classroom) {
	this.classroom = classroom;
}






}
