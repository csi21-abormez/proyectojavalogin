package com.spring.models;

import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
@Table(name = "Room")
public class Room extends DomainEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4631399647749478593L;

	private String name;
	private int capacity;
	
	public Room(String name, int capacity) {
		super();
		this.name = name;
		this.capacity = capacity;
	}
	public Room() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCapacity() {
		return capacity;
	}
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	
	
	
	// RELATIONS
	private List<Classroom> classes;
	
	@OneToMany(mappedBy = "room")
	@Valid
	@NotNull 
	public List<Classroom> getClasses() {
		return classes;
	}
	public void setClasses(List<Classroom> classes) {
		this.classes = classes;
	}
	
	
	

}

