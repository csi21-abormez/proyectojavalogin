package com.spring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.spring.models.Booking;
import com.spring.models.Classroom;

@Repository
public interface ClassroomRepository extends CrudRepository<Classroom, Long> {

	@Query(value = "SELECT cr FROM Classroom cr WHERE cr.classDate LIKE '%' ||:keyword || '%'"
			+ " OR cr.hour LIKE '%' || :keyword || '%'")
	 public List<Classroom> search(@Param("keyword") String keyword);
}
