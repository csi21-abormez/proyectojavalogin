package com.spring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.spring.models.Activity;
import com.spring.models.Customer;

@Repository
public interface ActivityRepository extends CrudRepository<Activity, Long> {
	@Query(value = "SELECT a FROM Activity a WHERE a.title LIKE '%' ||:keyword || '%'"
			 + " OR a.description LIKE '%' || :keyword || '%'")
			 public List<Activity> search(@Param("keyword") String keyword);

}
