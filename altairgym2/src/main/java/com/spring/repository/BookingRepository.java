package com.spring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.spring.models.Activity;
import com.spring.models.Booking;

@Repository
public interface BookingRepository extends CrudRepository<Booking, Long> {
	@Query(value = "SELECT b FROM Booking b WHERE b.bookingDate LIKE '%' ||:keyword || '%'")
			 public List<Booking> search(@Param("keyword") String keyword);

}
