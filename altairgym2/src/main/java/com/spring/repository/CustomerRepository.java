package com.spring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.spring.models.Customer;
import com.spring.models.Trainer;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {



	@Query(value = "SELECT c FROM Customer c WHERE c.name LIKE '%' ||:keyword || '%'"
			 + " OR c.email LIKE '%' || :keyword || '%'"
			 + " OR c.surname LIKE '%' || :keyword || '%'")
			 public List<Customer> search(@Param("keyword") String keyword);
	
	@Query(value ="SELECT count(1) FROM Customer c WHERE c.nick=:nick and c.password=:password")
	public Long pregunta(@Param("nick") String nick,@Param("password") String password);

	@Query(value = "SELECT c FROM Customer c WHERE c.nick=:nick and c.password=:password")
	public Customer dameCustomerLogado(@Param("nick") String nick, @Param("password") String password);
	
	@Query(nativeQuery = true, value ="SELECT * from Customer")
	public List<Customer> selectAll();
	
	/*
	 * @Query(value ="SELECT c FROM Customer c WHERE c.name LIKE ' ||:letra || '%'")
	 * public List<Customer> devuelvePorLetra(@Param("letra") String letra);
	 */

}
