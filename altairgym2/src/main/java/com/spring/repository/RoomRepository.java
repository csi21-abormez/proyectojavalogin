package com.spring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.spring.models.Room;



@Repository
public interface RoomRepository extends CrudRepository<Room, Long> {

	@Query(value = "SELECT r FROM Room r WHERE r.name LIKE '%' ||:keyword || '%'")
			 public List<Room> search(@Param("keyword") String keyword);

}
