package com.spring.repository;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.spring.models.Customer;
import com.spring.models.Trainer;

@Repository
public interface TrainerRepository extends CrudRepository<Trainer, Long> {
	
	@Query(value = "SELECT t FROM Trainer t WHERE t.name LIKE '%' ||:keyword || '%'"
			 + " OR t.email LIKE '%' || :keyword || '%'"
			 + " OR t.surname LIKE '%' || :keyword || '%'")
			 public List<Trainer> search(@Param("keyword") String keyword);

	@Query(value ="SELECT count(1) FROM Trainer t WHERE t.nick=:nick and t.password=:password")
	public Long pregunta(@Param("nick") String nick,@Param("password") String password);

	@Query(value = "SELECT t FROM Trainer t WHERE t.nick=:nick and t.password=:password")
	public Trainer dameTrainerLogado(@Param("nick") String nick, @Param("password") String password);
	
}
