package com.spring.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.models.Room;
import com.spring.models.Trainer;
import com.spring.repository.RoomRepository;

@Transactional
@Service
public class RoomServices {

	@Autowired
	RoomRepository roomRepository;
	
	public void save(Room room) {
		roomRepository.save(room);
		 }

		 public List<Room> listAll() {
		 return (List<Room>) roomRepository.findAll();
		 }

		 public Room get(Long id) {
		 return roomRepository.findById(id).get();
		 }

		 public void delete(Long id) {
			 roomRepository.deleteById(id);
		 }
		 
		 public List<Room> search(String keyword) {
			 return roomRepository.search(keyword);
			}
		 
	
}
