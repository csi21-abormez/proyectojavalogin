package com.spring.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.models.Booking;
import com.spring.repository.BookingRepository;

@Transactional
@Service
public class BookingServices {
	
	@Autowired
	 private BookingRepository bookingRepository;

	 public void save(Booking booking) {
		 bookingRepository.save(booking);
	 }

	 public List<Booking> listAll() {
	 return (List<Booking>) bookingRepository.findAll();
	 }

	 public Booking get(Long id) {
	 return bookingRepository.findById(id).get();
	 }

	 public void delete(Long id) {
		 bookingRepository.deleteById(id);
	 }
	 
	 public List<Booking> search(String keyword) {
		 return bookingRepository.search(keyword);
		}

}
