package com.spring.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.models.Customer;
import com.spring.models.Trainer;
import com.spring.repository.CustomerRepository;

@Service
@Transactional
public class CustomerServices {
	@Autowired
	 private CustomerRepository customerRepository;

	 public void save(Customer customer) {
		 customerRepository.save(customer);
	 }

	 public List<Customer> listAll() {
	 return (List<Customer>) customerRepository.findAll();
	 }

	 public Customer get(Long id) {
	 return customerRepository.findById(id).get();
	 }

	 public void delete(Long id) {
		 customerRepository.deleteById(id);
	 }
	 
	 public List<Customer> search(String keyword) {
		 return customerRepository.search(keyword);
		}

	 public Customer dameCustomerLogado(String usu, String clave) {
			return customerRepository.dameCustomerLogado(usu, clave);
			
		}
	 
//	 public List<Customer> devuelvePorLetra(String letra){
//		 return customerRepository.devuelvePorLetra(letra);
//	 }
	 
	

}
