package com.spring.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.models.Customer;
import com.spring.models.Trainer;
import com.spring.repository.TrainerRepository;

@Service
@Transactional
public class TrainerServices {

	@Autowired
	 private TrainerRepository trainerRepository;

	 public void save(Trainer trainer) {
	 trainerRepository.save(trainer);
	 }

	 public List<Trainer> listAll() {
	 return (List<Trainer>) trainerRepository.findAll();
	 }

	 public Trainer get(Long id) {
	 return trainerRepository.findById(id).get();
	 }

	 public void delete(Long id) {
		 trainerRepository.deleteById(id);
	 }
	 
	 public List<Trainer> search(String keyword) {
		 return trainerRepository.search(keyword);
		}
	 
	
	 public Trainer dameTrainerLogado(String usu, String clave) {
			return trainerRepository.dameTrainerLogado(usu, clave);
			
		}
}
