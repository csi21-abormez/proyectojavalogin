package com.spring.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.models.Activity;
import com.spring.models.Classroom;
import com.spring.repository.ClassroomRepository;

@Transactional
@Service
public class ClassroomServices {

	@Autowired
	 private ClassroomRepository classroomRepository;

	 public void save(Classroom classroom) {
		 classroomRepository.save(classroom);
	 }

	 public List<Classroom> listAll() {
	 return (List<Classroom>) classroomRepository.findAll();
	 }

	 public Classroom get(Long id) {
	 return classroomRepository.findById(id).get();
	 }

	 public void delete(Long id) {
		 classroomRepository.deleteById(id);
	 }
	 
	 public List<Classroom> search(String keyword) {
		 return classroomRepository.search(keyword);
		}
}
