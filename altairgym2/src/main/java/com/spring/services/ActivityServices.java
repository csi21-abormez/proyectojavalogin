package com.spring.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.models.Activity;
import com.spring.repository.ActivityRepository;

@Service
@Transactional
public class ActivityServices {
	
	@Autowired
	 private ActivityRepository activityRepository;

	 public void save(Activity activity) {
		 activityRepository.save(activity);
	 }

	 public List<Activity> listAll() {
	 return (List<Activity>) activityRepository.findAll();
	 }

	 public Activity get(Long id) {
	 return activityRepository.findById(id).get();
	 }

	 public void delete(Long id) {
		 activityRepository.deleteById(id);
	 }
	 
	 public List<Activity> search(String keyword) {
		 return activityRepository.search(keyword);
		}

}
