<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01
    Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Customer Manager</title>
</head>
<body>
<div align="center">
    <h2>Activity Manager</h2>
    <form method="post" action="activitySearch">
        <input type="text" name="keyword" /> &nbsp;
        <input type="submit" value="Search" />
    </form>
    <h3><a href="activityNew">New Activity</a></h3>
    <table border="1" cellpadding="5">
        <tr>
            
            <th>Title</th>
            <th>Description</th>
            <th>numberAssistants</th>
            <th>Duration</th>
            <th>Active</th>
            <th>Action</th>
        </tr>
        <c:forEach items="${listActivity}" var="activity">
        <tr>
            <td>${activity.title}</td>
            <td>${activity.description}</td>
            <td>${activity.numberAssistants}</td>
          	<td>${activity.duration}</td>
            <td>${activity.active}</td>
            <td>
                <a href="activityEdit?id=${activity.id}">Edit</a>
                &nbsp;&nbsp;&nbsp;
                <a href="activityDelete?id=${activity.id}">Delete</a>
            </td>
        </tr>
        </c:forEach>
    </table>
</div>   
</body>
</html>