<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
 pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>New Activity</title>
</head>
<body>
 <div align="center">
 <h2>New Activity</h2>
 <form:form action="activitySave" method="post" modelAttribute="activity">
 <table border="0" cellpadding="5">
<td>Title: </td>
 <td><form:input path="title" /></td>
 </tr>
 <tr>
 <td>Description: </td>
 <td><form:input path="description" /></td>
 </tr>
 <tr>
 <td>numberAssistants: </td>
 <td><form:input path="numberAssistants" /></td>
 </tr>
 
 <td>Duration: </td>
 <td><form:input path="duration" /></td>
 </tr>
 <tr>
 <td>Active: </td>
 <td><form:input path="active" /></td>
 </tr>
 <tr>
 <td colspan="2"><input type="submit" value="Save"></td>
 </tr>
 </table>
 </form:form>
 </div>
</body>
</html>