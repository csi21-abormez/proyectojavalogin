<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01
    Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Customer Manager</title>
</head>
<body>
<div align="center">
    <h2>Trainer Manager</h2>
    <form method="post" action="trainerSearch">
        <input type="text" name="keyword" /> &nbsp;
        <input type="submit" value="Search" />
    </form>
    <h3><a href="trainerNew">New trainer</a></h3>
    <table border="1" cellpadding="5">
        <tr>
           <th>ID</th>
            <th>Name</th>
            <th>E-mail</th>
            <th>Surname</th>
            <th>Phone</th>
            <th>Age</th>
            <th>Action</th>
        </tr>
        <div>
        <ul>
       	<li> <a href="activityHome">Pulse para configurar actividad</a></li>
       	<li> <a href="roomHome">Pulse para configurar una sala</a></li>
       	<li> <a href="classroomHome">Pulse para configurar una clase</a></li>
       	
        </ul>
        </div>
        <c:forEach items="${listTrainer}" var="trainer">
        <tr>
            <td>${trainer.id}</td>
            <td>${trainer.name}</td>
            <td>${trainer.email}</td>
            <td>${trainer.surname}</td>
            <td>${trainer.phone}</td>
            <td>${trainer.age}</td>
            
            <td>
                <a href="trainerEdit?id=${trainer.id}">Edit</a>
                &nbsp;&nbsp;&nbsp;
                <a href="trainerDelete?id=${trainer.id}">Delete</a>
            </td>
        </tr>
        </c:forEach>
    </table>
</div>   
</body>
</html>