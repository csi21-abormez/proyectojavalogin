<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01
    Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Classroom Manager</title>
</head>
<body>
<div align="center">
    <h2>Classroom Manager</h2>
    <form method="post" action="classroomSearch">
        <input type="text" name="keyword" /> &nbsp;
        <input type="submit" value="Search" />
    </form>
    <h3><a href="classroomNew">New Classroom</a></h3>
    <table border="1" cellpadding="5">
        <tr>
            
            <th>Class Date</th>
            <th>Hour</th>
            <th>Action</th>
        </tr>
        <c:forEach items="${listClassroom}" var="classroom">
        <tr>
            <td>${classroom.classDate}</td>
            <td>${classroom.hour}</td>
          
                <a href="classroomEdit?id=${classroom.id}">Edit</a>
                &nbsp;&nbsp;&nbsp;
                <a href="classroomDelete?id=${classroom.id}">Delete</a>
            </td>
        </tr>
        </c:forEach>
    </table>
</div>   
</body>
</html>