<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01
    Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Room Manager</title>
</head>
<body>
<div align="center">
    <h2>Room Manager</h2>
    <form method="post" action="roomSearch">
        <input type="text" name="keyword" /> &nbsp;
        <input type="submit" value="Search" />
    </form>
    <h3><a href="roomNew">New Room</a></h3>
    <table border="1" cellpadding="5">
        <tr>
            
            <th>Name</th>
            <th>Capacity</th>
          
            <th>Action</th>
        </tr>
        <c:forEach items="${listRoom}" var="room">
        <tr>
            <td>${room.name}</td>
            <td>${room.capacity}</td>
            
                <a href="roomactivityEdit?id=${room.id}">Edit</a>
                &nbsp;&nbsp;&nbsp;
                <a href="roomDelete?id=${room.id}">Delete</a>
            </td>
        </tr>
        </c:forEach>
    </table>
</div>   
</body>
</html>