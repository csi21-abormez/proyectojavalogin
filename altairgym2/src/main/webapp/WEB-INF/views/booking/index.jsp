<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01
    Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Booking Manager</title>
</head>
<body>
<div align="center">
    <h2>Booking Manager</h2>
    <form method="post" action="bookingSearch">
        <input type="text" name="keyword" /> &nbsp;
        <input type="submit" value="Search" />
    </form>
    <h3><a href="bookingNew">New Booking</a></h3>
    <table border="1" cellpadding="5">
        <tr>
            
            <th>BookingDate</th>
            
        </tr>
        <c:forEach items="${listBooking}" var="booking">
        <tr>
            <td>${booking.bookingDate}</td>
           
            
                <a href="bookingEdit?id=${booking.id}">Edit</a>
                &nbsp;&nbsp;&nbsp;
                <a href="bookingDelete?id=${booking.id}">Delete</a>
            </td>
        </tr>
        </c:forEach>
    </table>
</div>   
</body>
</html>